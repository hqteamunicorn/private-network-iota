FROM openjdk:jre-slim
MAINTAINER Anja
COPY iri.jar /iri.jar
EXPOSE 14265
EXPOSE 14777/udp
EXPOSE 15777
CMD ["/usr/bin/java", "-XX:+DisableAttachMechanism", "-Xmx8g", "-Xms256m", "-Dlogback.configurationFile=/iri/conf/logback.xml", "-Djava.net.preferIPv4Stack=true", "-jar", "iri.jar", "-p", "14265", "-u", "14777", "-t", "15777", "--remote", "$@", "--testnet"]